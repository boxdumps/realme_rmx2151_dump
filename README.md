## RQ2A.210405.006
- Manufacturer: realme
- Platform: 
- Codename: RMX2151
- Brand: realme
- Flavor: lineage_RMX2151-userdebug
- Release Version: 11
- Id: RQ2A.210405.006
- Incremental: eng.ksaura.20210418.185458
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/lineage_RMX2151/RMX2151:11/RQ2A.210405.006/ksauraj104181852:userdebug/test-keys
- OTA version: 
- Branch: RQ2A.210405.006
- Repo: realme_rmx2151_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
